import matplotlib.pyplot as plt
import numpy as np

t = np.linspace(0, 1, 100)
Ca = -1*t+1
plt.plot(t, Ca, '-r', label='Ca=-kt + Ca0')
plt.title('Graph of Ca=-kt + Ca0')
plt.xlabel('t', color='#1C2833')
plt.ylabel('Ca', color='#1C2833')
plt.legend(loc='upper left')
plt.grid()
plt.show()